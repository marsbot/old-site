import sys
sys.path.append('.')

AUTHOR = 'marsbot'
SITENAME = 'Marsbot'
SITEURL = ''

PATH = 'content'
PAGE_PATHS = ['pages']
OUTPUT_PATH = 'public'
THEME = 'themes/machines'
CSS_FILE = 'style.css'

TIMEZONE = 'America/Mexico_City'
DEFAULT_LANG = 'es'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
#LINKS = (('Pelican', 'http://getpelican.com/'),
#         ('Python.org', 'http://python.org/'),
#         ('Jinja2', 'http://jinja.pocoo.org/'),
#         ('You can modify those links in your config file', '#'),)
# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

RELATIVE_URLS = True

DIRECT_TEMPLATES = ['index', 'blog']
PAGINATED_DIRECT_TEMPLATES = ['blog']

ARTICLE_URL = "blog/{slug}"
ARTICLE_SAVE_AS = "blog/{slug}/index.html"

AUTHOR_URL = ''
AUTHOR_SAVE_AS = ''

#ARCHIVES_URL = "archives"
#ARCHIVES_SAVE_AS = "archives/index.html"

BLOG_URL = 'blog'
BLOG_SAVE_AS = 'blog/index.html'

PAGE_URL = '{slug}'
PAGE_SAVE_AS = '{slug}/index.html'

CATEGORY_URL = ''
CATEGORY_SAVE_AS = ''

TAG_URL = ''
TAG_SAVE_AS = ''

USE_FOLDER_AS_CATEGORY = True

# Generate yearly archive
#YEAR_ARCHIVE_SAVE_AS = 'blog/{date:%Y}/index.html'

# Show most recent posts first
NEWEST_FIRST_ARCHIVES = True

STATIC_PATHS = ['images',
                'fonts',
                'css',
                'js',
                ]
EXTRA_PATH_METADATA = {
    'images/Favicon.png': {'path': 'Favicon.png'}
}

from jinja2 import nodes
from jinja2.ext import Extension

import re


class SpacelessExtension(Extension):
    tags = set(['spaceless'])

    def parse(self, parser):
        lineno = next(parser.stream).lineno
        body = parser.parse_statements(['name:endspaceless'], drop_needle=True)
        return nodes.CallBlock(
            self.call_method('_strip_spaces', [], [], None, None),
            [], [], body,
        ).set_lineno(lineno)

    def _strip_spaces(self, caller=None):
        return re.sub(r'>\s+<', '><', caller().strip())

JINJA_EXTENSIONS = [SpacelessExtension]

PLUGIN_PATHS = ['plugins']
PLUGINS = ['assets']

ASSET_CONFIG = (('CLEANCSS_EXTRA_ARGS', ('skip-rebase',)),)
