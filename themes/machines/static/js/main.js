(function() {
  var slider = document.getElementById('up-slide');

  var moveElement = function() {
    var firstElement = document.getElementById('up-slide').childNodes[0];
    firstElement.style.marginTop = "-" + (firstElement.parentElement.offsetHeight) + "px";
  }

  var animateSlide = function() {
    var firstElement = document.getElementById('up-slide').childNodes[0];
    slider.removeChild(firstElement);
    moveElement();
    slider.appendChild(firstElement);
    firstElement.style.marginTop = 0;
  }

  moveElement();
  setInterval(animateSlide, 3500);
})()
