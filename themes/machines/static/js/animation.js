(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 1980,
	height: 132,
	fps: 24,
	color: "#0066CC",
	opacity: 1.00,
	manifest: []
};



lib.ssMetadata = [];


// symbols:



(lib.WR = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAABMQhZAAhUAmQgGgZAAgXQAAhJA1g1QA1g1BJAAQBKAAA1A1QA1A1AABJQAAAXgGAZQhUgmhaAAg");
	this.shape.setTransform(9.7,7.8,0.284,0.284);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#DEDEDE").s().p("Ah+B/Qg1g1AAhKQAAhJA1g1QA1g1BJAAQBKAAA1A1QA1A1AABJQAABKg1A1Qg1A1hKAAQhJAAg1g1g");
	this.shape_1.setTransform(9.7,9.7,0.284,0.284);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#343034").s().p("AjxDyQhlhkAAiOQAAiNBlhkQBkhlCNAAQCOAABkBlQBlBkAACNQAACOhlBkQhkBliOAAQiNAAhkhlg");
	this.shape_2.setTransform(9.7,9.7,0.284,0.284);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,19.5,19.5);


(lib.WL = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAABMQhZAAhUAmQgGgZAAgXQAAhJA1g1QA1g1BJAAQBKAAA1A1QA1A1AABJQAAAagGAWQhUgmhaAAg");
	this.shape.setTransform(9.7,7.8,0.284,0.284);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#DEDEDE").s().p("Ah+B/Qg1g1AAhKQAAhJA1g1QA1g1BJAAQBKAAA1A1QA1A1AABJQAABKg1A1Qg1A1hKAAQhJAAg1g1g");
	this.shape_1.setTransform(9.7,9.7,0.284,0.284);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#343034").s().p("AjxDyQhlhkAAiOQAAiNBlhkQBkhlCNAAQCOAABkBlQBlBkAACNQAACOhlBkQhkBliOAAQiNAAhkhlg");
	this.shape_2.setTransform(9.7,9.7,0.284,0.284);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,19.5,19.5);


(lib.Cloud = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AttKWIgNgCIgOgCIgCgCQhygBhPhPQhQhQAAhxQAAhxBQhQQBPhPByAAQArAAAYALIAUAAQgUgkgHg0QgIguAEg0QAUj8ClipQCpiwDyAAQDRAACYCIQCTCDAsDPQAcgPAtgMQAtgKAjAAQBpAABJBKQBKBIAABnQAAAngHAjIgKAnIAXgNQBKg6BoAAQB5AABXBWQBVBXAAB6QAABCgcA8QgbA6gwApQghAigqAUQgtAVgwAAg");
	this.shape.setTransform(64.5,36.2,0.546,0.546);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,129,72.4);


(lib.AutoAn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#6B6B6B").ss(0.1).p("AhmAXIAbg4ICngFIAKAOIgOARQgDgCgHgBQgOgBgUAJQgTAGgLADIgHABIgXAPIgJARIhEAAg");
	this.shape.setTransform(56.6,24.9,0.284,0.284);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D0D0D0").s().p("AhdAnIgJgQIAcg4ICngGIAKAPIgOARQgDgDgIAAQgOgBgTAIQgUAHgLADIgGABIgXAPIgJAQg");
	this.shape_1.setTransform(56.6,24.9,0.284,0.284);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#B24607").ss(0.3).p("Ahsh7IAPALQAZAWAxA/QAvA9AbAxQAOAYAEALIAwhHQgNgegLgrQgHgbgCggIgCgag");
	this.shape_2.setTransform(92.1,35.4,0.284,0.284,0,0,0,0.7,0.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#ED5D09").s().p("AAxBWQgbgxgvg9Qgxg/gZgWIgPgLIDCAMIACAaQACAgAHAbQALArANAeIgwBHQgEgMgOgXg");
	this.shape_3.setTransform(92.1,35.2,0.284,0.284);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#71B702").p("AIRAwIwlAAIAAg4IPUAAIABghIAkAgQANANAQAVg");
	this.shape_4.setTransform(75.4,21.6,0.284,0.284);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#8ED246").s().p("AoSAtIAAg4IPUAAIABghIAkAgQANANAQAVIAPAXg");
	this.shape_5.setTransform(75.3,21.7,0.284,0.284);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#71B702").p("ACGCBIjvAAIgbkBIEKAAg");
	this.shape_6.setTransform(46.8,36);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#8ED246").s().p("AhpCBIgbkBIEJAAIAAEBg");
	this.shape_7.setTransform(46.9,36);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#888189").p("AocEaIAdpxIjNAIIjKJhIgEAIIgXA+IdlAAIAAg+gAqpkhIBxAAIgUI7IkWAAg");
	this.shape_8.setTransform(61.5,11.6,0.284,0.284);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#6A616B").s().p("AuyFYIAXg+IAEgIIDKphIDNgIIgdJxIXQAAIAAA+gAtjEaIEWAAIAUo7IhxAAg");
	this.shape_9.setTransform(61.5,11.6,0.284,0.284);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#816C44").p("AlLhpQAEgEFKgmQCmgSCmgSIgEBFQgFBKgNAUQgPAWgcAZQgLAJgfAaQgKAHgZARQgdASgPAMQgcAVgaAaIgUAWImWAAQgChHgBhJQgBiOAEgEg");
	this.shape_10.setTransform(80.4,14.9,0.284,0.284);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#AC905B").s().p("AlLC4IgDiPQgBiOAEgEQAEgFFKglIFMglIgEBGQgFBJgNAVQgPAWgcAZIgqAjQgKAHgZAQQgdATgPAMQgcAVgaAaIgUAVg");
	this.shape_11.setTransform(80.4,14.9,0.284,0.284);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#816C44").p("AhcCDQAIg1AKg5QAThvAJgNQAKgNA7gPQAfgIAcgFIALAAIgJEhIiwAAg");
	this.shape_12.setTransform(63.9,15.5,0.284,0.284);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#AC905B").s().p("AhcCRIAAgNIAShuQAThwAJgMQAKgNA7gQQAfgIAcgFIALAAIgJEhg");
	this.shape_13.setTransform(63.9,15.5,0.284,0.284);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#6B6B6B").ss(0.1).p("ABag8Qg2g6hrgyIhHFLQBrASBMgUQBOgUATg1QAMghgMgoQgNgmgjglg");
	this.shape_14.setTransform(4.1,35.4,0.284,0.284);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#D0D0D0").s().p("AiOCiIBHlLQBqAyA3A6QAjAlAMAmQANAogMAhQgTA1hPAUQgoAKgwAAQgsAAgygIg");
	this.shape_15.setTransform(4.1,35.5,0.284,0.284);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#4D8710").s().p("AmLDTQAQjJB9iDQB9iGCgALQCeAKBqCPQBqCNgFDDg");
	this.shape_16.setTransform(78.6,42.9,0.284,0.284);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#4D8710").s().p("AkUhrQB0iXCgAAQCeAAByCRQBzCPAJDTIsWASQADjYBziWg");
	this.shape_17.setTransform(19.3,42.3,0.284,0.284);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#6B6B6B").ss(0.1).p("Ag0CjQBHgTAJgHQAIgHgCguQAFg3AFg4QAKhzgBgIQgBgJgLgCIgLgBIg6ADIgOCYQgKByAAA4g");
	this.shape_18.setTransform(31.5,20.1,0.284,0.284);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#D0D0D0").s().p("AgqgHIAOiYIA6gDIALABQALACABAJQABAIgKBzIgKBvQACAugIAHQgJAHhHATQAAg4AKhyg");
	this.shape_19.setTransform(31.5,20.1,0.284,0.284);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#9A98AB").s().p("AhUBCQgUgGgEgKIgGgUQgFgPADgEQAGgIB4g2QAmgSAaAAQAcAAAMAEIAEAJQACALgGALQgKAPgZAUIgKAIQhFAtgYAJQgUAHgSAAQgMAAgKgEg");
	this.shape_20.setTransform(27.2,22.1,0.284,0.284);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#71B702").p("AiPlNIAnAAQABAAAFgrIhugbIjZIPIhdEZIQPAAIAAg/QnUgBiUACIkRAAg");
	this.shape_21.setTransform(45.2,11.5,0.284,0.284);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#8ED246").s().p("AoHGUIBdkYIDZoQIBuAcIgGArIgnAAIjhKkIERAAQCUgCHUAAIAAA/g");
	this.shape_22.setTransform(45.2,11.5,0.284,0.284);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#A3D96D").s().p("Ao5F6QgTkaBzjRQB0jWDIgqQBxgXByApQBwAnBkBeQDSDGBFFHQAGAfAGAog");
	this.shape_23.setTransform(81,38.2,0.284,0.284);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#A3D96D").s().p("AnyF8IAAiEIhigMQAwkGCjiuQCnizDTAAQD1AACwDeQCwDcAHE9g");
	this.shape_24.setTransform(19.6,38,0.284,0.284);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#71B702").p("AKnEIIjcAHIgHDEIxrgaIAAuMIQkAAIB3DVICGAmIgqCdIAuCdg");
	this.shape_25.setTransform(79.6,36.3,0.284,0.284);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#8ED246").s().p("AqnG5IAAuMIQlAAIB2DVICGAmIgpCeIAuCcIAoCnIjbAGIgHDEg");
	this.shape_26.setTransform(79.6,36.3,0.284,0.284);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#71B702").p("AnSFmIgDCFIRAAAIhsvAQhjgVhhAAQlmAAkMDwQh/BxhPCUQhRCbgUCtg");
	this.shape_27.setTransform(18.8,34.9,0.284,0.284);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#8ED246").s().p("AnVHrIADiFIiYgTQAUitBRibQBPiUB/hxQEMjwFmAAQBhAABjAVIBsPAg");
	this.shape_28.setTransform(18.8,34.9,0.284,0.284);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-1,100.9,52);


(lib.Car = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// WR
	this.instance = new lib.WR();
	this.instance.setTransform(78.3,50.2,1,1,0,0,0,9.8,9.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:9.7,regY:9.7,rotation:-10,x:78.2,y:50.1},0).wait(1).to({rotation:-19.9},0).wait(1).to({rotation:-29.9},0).wait(1).to({rotation:-39.8,y:50.2},0).wait(1).to({rotation:-49.8,y:50.1},0).wait(1).to({rotation:-59.7,y:50.2},0).wait(1).to({rotation:-69.7},0).wait(1).to({rotation:-77.9},0).wait(1).to({rotation:-86.1},0).wait(1).to({rotation:-94.3},0).wait(1).to({rotation:-102.5},0).wait(1).to({rotation:-110.8},0).wait(1).to({rotation:-119},0).wait(1).to({rotation:-127.2,x:78.3},0).wait(1).to({rotation:-135.4,x:78.2},0).wait(1).to({rotation:-143.6,x:78.3},0).wait(1).to({rotation:-151.9},0).wait(1).to({rotation:-160.1},0).wait(1).to({rotation:-168.3},0).wait(1).to({rotation:-176.6},0).wait(1).to({rotation:-184.8},0).wait(1).to({rotation:-192.5},0).wait(1).to({rotation:-200.3},0).wait(1).to({rotation:-208},0).wait(1).to({rotation:-215.7,y:50.1},0).wait(1).to({rotation:-223.4},0).wait(1).to({rotation:-231.2},0).wait(1).to({rotation:-238.9},0).wait(1).to({rotation:-246.6},0).wait(1).to({rotation:-255.8},0).wait(1).to({rotation:-265.1},0).wait(1).to({rotation:-274.3},0).wait(1).to({rotation:-283.5,x:78.2},0).wait(1).to({rotation:-292.8,x:78.3},0).wait(1).to({rotation:-302},0).wait(1).to({rotation:-312.4},0).wait(1).to({rotation:-322.8},0).wait(1).to({rotation:-333.2,x:78.2},0).wait(1).to({rotation:-343.6},0).wait(1).to({rotation:-354},0).wait(1));

	// WL
	this.instance_1 = new lib.WL();
	this.instance_1.setTransform(19.3,50.2,1,1,0,0,0,9.8,9.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({regX:9.7,regY:9.7,rotation:-8.6,x:19.2,y:50.1},0).wait(1).to({rotation:-17.1,x:19.1},0).wait(1).to({rotation:-25.7},0).wait(1).to({rotation:-34.3},0).wait(1).to({rotation:-42.9},0).wait(1).to({rotation:-51.4},0).wait(1).to({rotation:-60,y:50.2},0).wait(1).to({rotation:-68.1},0).wait(1).to({rotation:-76.1},0).wait(1).to({rotation:-84.2,x:19.2},0).wait(1).to({rotation:-92.3,x:19.1},0).wait(1).to({rotation:-100.4,x:19.2},0).wait(1).to({rotation:-108.4},0).wait(1).to({rotation:-116.5},0).wait(1).to({rotation:-125.4},0).wait(1).to({rotation:-134.4},0).wait(1).to({rotation:-143.3},0).wait(1).to({rotation:-152.2},0).wait(1).to({rotation:-161.1},0).wait(1).to({rotation:-170.1},0).wait(1).to({rotation:-179},0).wait(1).to({rotation:-186.5},0).wait(1).to({rotation:-193.9,x:19.3},0).wait(1).to({rotation:-201.4,x:19.2},0).wait(1).to({rotation:-208.8},0).wait(1).to({rotation:-216.3,x:19.3},0).wait(1).to({rotation:-223.8,y:50.1},0).wait(1).to({rotation:-231.2},0).wait(1).to({rotation:-238.7,x:19.2},0).wait(1).to({rotation:-248.5},0).wait(1).to({rotation:-258.4,x:19.3},0).wait(1).to({rotation:-268.2,x:19.2},0).wait(1).to({rotation:-278.1},0).wait(1).to({rotation:-288},0).wait(1).to({rotation:-297.8},0).wait(1).to({rotation:-309.9},0).wait(1).to({rotation:-321.9},0).wait(1).to({rotation:-333.9},0).wait(1).to({rotation:-346},0).wait(1).to({rotation:-358,x:19.1},0).wait(1));

	// AutoAn
	this.instance_2 = new lib.AutoAn();
	this.instance_2.setTransform(49.7,25.2,1,1,0,0,0,49.7,25.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1).to({regX:49.4,regY:24.8,x:49.4,y:24.8},0).wait(1).to({y:24.9},0).wait(2).to({y:25},0).wait(2).to({y:25.1},0).wait(2).to({y:25.2},0).wait(2).to({y:25.3},0).wait(2).to({y:25.4},0).wait(2).to({y:25.5},0).wait(2).to({y:25.6},0).wait(2).to({y:25.7},0).wait(2).to({y:25.8},0).wait(2).to({y:25.9},0).wait(1).to({y:25.8},0).wait(1).to({y:25.7},0).wait(2).to({y:25.6},0).wait(2).to({y:25.5},0).wait(1).to({y:25.4},0).wait(2).to({y:25.3},0).wait(2).to({y:25.2},0).wait(2).to({y:25.1},0).wait(1).to({y:25},0).wait(2).to({y:24.9},0).wait(2).to({y:24.8},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-0.6,99.6,60.5);


// stage content:



(lib.Animation = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Car (1).ai
	this.instance = new lib.Car();
	this.instance.setTransform(2045.1,101.8,1,1,0,0,0,49.7,29.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:49.4,regY:31.9,x:2036.4,y:104},0).wait(1).to({x:2028.1,y:104.1},0).wait(1).to({x:2019.7},0).wait(1).to({x:2011.4,y:104.2},0).wait(1).to({x:2003},0).wait(1).to({x:1994.7},0).wait(1).to({x:1986.3,y:104.3},0).wait(1).to({x:1978},0).wait(1).to({x:1969.6},0).wait(1).to({x:1961.3,y:104.4},0).wait(1).to({x:1953},0).wait(1).to({x:1944.6,y:104.5},0).wait(1).to({x:1936.3},0).wait(1).to({x:1927.9},0).wait(1).to({x:1919.6,y:104.6},0).wait(1).to({x:1911.2},0).wait(1).to({x:1902.9,y:104.7},0).wait(1).to({x:1894.5},0).wait(1).to({x:1886.2},0).wait(1).to({x:1877.8,y:104.8},0).wait(1).to({x:1869.5},0).wait(1).to({x:1861.2,y:104.9},0).wait(1).to({x:1852.8},0).wait(1).to({x:1844.5},0).wait(1).to({x:1836.1,y:105},0).wait(1).to({x:1827.8},0).wait(1).to({x:1819.4},0).wait(1).to({x:1811.1,y:105.1},0).wait(1).to({x:1802.7},0).wait(1).to({x:1794.4,y:105.2},0).wait(1).to({x:1786},0).wait(1).to({x:1777.7},0).wait(1).to({x:1769.4,y:105.3},0).wait(1).to({x:1761},0).wait(1).to({x:1752.7,y:105.4},0).wait(1).to({x:1744.3},0).wait(1).to({x:1736},0).wait(1).to({x:1727.6,y:105.5},0).wait(1).to({x:1719.3},0).wait(1).to({x:1710.9},0).wait(1).to({x:1702.6,y:105.6},0).wait(1).to({x:1694.2},0).wait(1).to({x:1685.9,y:105.7},0).wait(1).to({x:1677.6},0).wait(1).to({x:1669.2},0).wait(1).to({x:1660.9,y:105.8},0).wait(1).to({x:1652.5},0).wait(1).to({x:1644.2,y:105.9},0).wait(1).to({x:1635.8},0).wait(1).to({x:1627.5},0).wait(1).to({x:1619.1,y:106},0).wait(1).to({x:1610.8},0).wait(1).to({x:1602.4,y:106.1},0).wait(1).to({x:1594.1},0).wait(1).to({x:1585.8},0).wait(1).to({x:1577.4,y:106.2},0).wait(1).to({x:1569.1},0).wait(1).to({x:1560.7},0).wait(1).to({x:1552.4,y:106.3},0).wait(1).to({x:1544},0).wait(1).to({x:1535.7,y:106.4},0).wait(1).to({x:1527.3},0).wait(1).to({x:1519},0).wait(1).to({x:1510.6,y:106.5},0).wait(1).to({x:1502.3},0).wait(1).to({x:1494,y:106.6},0).wait(1).to({x:1485.6},0).wait(1).to({x:1477.3},0).wait(1).to({x:1468.9,y:106.7},0).wait(1).to({x:1460.6},0).wait(1).to({x:1452.2},0).wait(1).to({x:1443.9,y:106.8},0).wait(1).to({x:1435.5},0).wait(1).to({x:1427.2,y:106.9},0).wait(1).to({x:1418.8},0).wait(1).to({x:1410.5},0).wait(1).to({x:1402.2,y:107},0).wait(1).to({x:1393.8},0).wait(1).to({x:1385.5,y:107.1},0).wait(1).to({x:1377.1},0).wait(1).to({x:1368.8},0).wait(1).to({x:1360.4,y:107.2},0).wait(1).to({x:1352.1},0).wait(1).to({x:1343.7,y:107.3},0).wait(1).to({x:1335.4},0).wait(1).to({x:1327},0).wait(1).to({x:1318.7,y:107.4},0).wait(1).to({x:1310.4},0).wait(1).to({x:1302},0).wait(1).to({x:1293.7,y:107.5},0).wait(1).to({x:1285.3},0).wait(1).to({x:1277,y:107.6},0).wait(1).to({x:1268.6},0).wait(1).to({x:1260.3},0).wait(1).to({x:1251.9,y:107.7},0).wait(1).to({x:1243.6},0).wait(1).to({x:1235.2,y:107.8},0).wait(1).to({x:1226.9},0).wait(1).to({x:1218.6},0).wait(1).to({x:1210.2,y:107.9},0).wait(1).to({x:1201.9},0).wait(1).to({x:1193.5},0).wait(1).to({x:1185.2,y:108},0).wait(1).to({x:1176.8},0).wait(1).to({x:1168.5,y:108.1},0).wait(1).to({x:1160.1},0).wait(1).to({x:1151.8},0).wait(1).to({x:1143.4,y:108.2},0).wait(1).to({x:1135.1},0).wait(1).to({x:1126.8,y:108.3},0).wait(1).to({x:1118.4},0).wait(1).to({x:1110.1},0).wait(1).to({x:1101.7,y:108.4},0).wait(1).to({x:1093.4},0).wait(1).to({x:1085},0).wait(1).to({x:1076.7,y:108.5},0).wait(1).to({x:1068.3},0).wait(1).to({x:1060,y:108.6},0).wait(1).to({x:1051.6},0).wait(1).to({x:1043.3},0).wait(1).to({x:1035,y:108.7},0).wait(1).to({x:1026.6},0).wait(1).to({x:1018.3,y:108.8},0).wait(1).to({x:1009.9},0).wait(1).to({x:1001.6},0).wait(1).to({x:993.2,y:108.9},0).wait(1).to({x:984.9},0).wait(1).to({x:976.5,y:109},0).wait(1).to({x:968.2},0).wait(1).to({x:959.8},0).wait(1).to({x:951.5,y:109.1},0).wait(1).to({x:943.2},0).wait(1).to({x:934.8},0).wait(1).to({x:926.5,y:109.2},0).wait(1).to({x:918.1},0).wait(1).to({x:909.8,y:109.3},0).wait(1).to({x:901.4},0).wait(1).to({x:893.1},0).wait(1).to({x:884.7,y:109.4},0).wait(1).to({x:876.4},0).wait(1).to({x:868,y:109.5},0).wait(1).to({x:859.7},0).wait(1).to({x:851.4},0).wait(1).to({x:843,y:109.6},0).wait(1).to({x:834.7},0).wait(1).to({x:826.3},0).wait(1).to({x:818,y:109.7},0).wait(1).to({x:809.6},0).wait(1).to({x:801.3,y:109.8},0).wait(1).to({x:792.9},0).wait(1).to({x:784.6},0).wait(1).to({x:776.2,y:109.9},0).wait(1).to({x:767.9},0).wait(1).to({x:759.6,y:110},0).wait(1).to({x:751.2},0).wait(1).to({x:742.9},0).wait(1).to({x:734.5,y:110.1},0).wait(1).to({x:726.2},0).wait(1).to({x:717.8,y:110.2},0).wait(1).to({x:709.5},0).wait(1).to({x:701.1},0).wait(1).to({x:692.8,y:110.3},0).wait(1).to({x:684.4},0).wait(1).to({x:676.1},0).wait(1).to({x:667.8,y:110.4},0).wait(1).to({x:659.4},0).wait(1).to({x:651.1,y:110.5},0).wait(1).to({x:642.7},0).wait(1).to({x:634.4},0).wait(1).to({x:626,y:110.6},0).wait(1).to({x:617.7},0).wait(1).to({x:609.3,y:110.7},0).wait(1).to({x:601},0).wait(1).to({x:592.6},0).wait(1).to({x:584.3,y:110.8},0).wait(1).to({x:576},0).wait(1).to({x:567.6},0).wait(1).to({x:559.3,y:110.9},0).wait(1).to({x:550.9},0).wait(1).to({x:542.6,y:111},0).wait(1).to({x:534.2},0).wait(1).to({x:525.9},0).wait(1).to({x:517.5,y:111.1},0).wait(1).to({x:509.2},0).wait(1).to({x:500.8,y:111.2},0).wait(1).to({x:492.5},0).wait(1).to({x:484.2},0).wait(1).to({x:475.8,y:111.3},0).wait(1).to({x:467.5},0).wait(1).to({x:459.1,y:111.4},0).wait(1).to({x:450.8},0).wait(1).to({x:442.4},0).wait(1).to({x:434.1,y:111.5},0).wait(1).to({x:425.7},0).wait(1).to({x:417.4},0).wait(1).to({x:409,y:111.6},0).wait(1).to({x:400.7},0).wait(1).to({x:392.4,y:111.7},0).wait(1).to({x:384},0).wait(1).to({x:375.7},0).wait(1).to({x:367.3,y:111.8},0).wait(1).to({x:359},0).wait(1).to({x:350.6,y:111.9},0).wait(1).to({x:342.3},0).wait(1).to({x:333.9},0).wait(1).to({x:325.6,y:112},0).wait(1).to({x:317.2},0).wait(1).to({x:308.9},0).wait(1).to({x:300.6,y:112.1},0).wait(1).to({x:292.2},0).wait(1).to({x:283.9,y:112.2},0).wait(1).to({x:275.5},0).wait(1).to({x:267.2},0).wait(1).to({x:258.8,y:112.3},0).wait(1).to({x:250.5},0).wait(1).to({x:242.1,y:112.4},0).wait(1).to({x:233.8},0).wait(1).to({x:225.4},0).wait(1).to({x:217.1,y:112.5},0).wait(1).to({x:208.8},0).wait(1).to({x:200.4,y:112.6},0).wait(1).to({x:192.1},0).wait(1).to({x:183.7},0).wait(1).to({x:175.4,y:112.7},0).wait(1).to({x:167},0).wait(1).to({x:158.7},0).wait(1).to({x:150.3,y:112.8},0).wait(1).to({x:142},0).wait(1).to({x:133.6,y:112.9},0).wait(1).to({x:125.3},0).wait(1).to({x:117},0).wait(1).to({x:108.6,y:113},0).wait(1).to({x:100.3},0).wait(1).to({x:91.9,y:113.1},0).wait(1).to({x:83.6},0).wait(1).to({x:75.2},0).wait(1).to({x:66.9,y:113.2},0).wait(1).to({x:58.5},0).wait(1).to({x:50.2},0).wait(1).to({x:41.9,y:113.3},0).wait(1).to({x:33.5},0).wait(1).to({x:25.2,y:113.4},0).wait(1).to({x:16.9},0).wait(1).to({x:8.5},0).wait(1).to({x:0.2,y:113.5},0).wait(1).to({x:-8.2},0).wait(1).to({x:-16.5,y:113.6},0).wait(1).to({x:-24.9},0).wait(1).to({x:-33.2},0).wait(1).to({x:-41.6,y:113.7},0).wait(1).to({x:-49.9},0).wait(1).to({x:-58.3,y:113.8},0).wait(1));

	// Cloud.ai
	this.instance_1 = new lib.Cloud();
	this.instance_1.setTransform(-235.2,39.2,1,1,0,0,0,64.5,36.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(253));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(690.3,69,2394.5,129);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;


var canvas, stage, exportRoot;
function init() {
	canvas = document.getElementById("car");
	handleComplete();
}
function handleComplete() {
	//This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
	exportRoot = new lib.Animation();
	stage = new createjs.Stage(canvas);
	stage.addChild(exportRoot);
	//Registers the "tick" event listener.
	createjs.Ticker.setFPS(lib.properties.fps);
	createjs.Ticker.addEventListener("tick", stage);
	//Code to support hidpi screens and responsive scaling.
	(function(isResp, respDim, isScale, scaleType) {
		var lastW, lastH, lastS=1;
		window.addEventListener('resize', resizeCanvas);
		resizeCanvas();
		function resizeCanvas() {
			var w = lib.properties.width, h = lib.properties.height;
			var iw = window.innerWidth, ih=window.innerHeight;
			var pRatio = window.devicePixelRatio, xRatio=iw/w, yRatio=ih/h, sRatio=1;
			if(isResp) {
				if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
					sRatio = lastS;
				}
				else if(!isScale) {
					if(iw<w || ih<h)
						sRatio = Math.min(xRatio, yRatio);
				}
				else if(scaleType==1) {
					sRatio = Math.min(xRatio, yRatio);
				}
				else if(scaleType==2) {
					sRatio = Math.max(xRatio, yRatio);
				}
			}
			canvas.width = iw*pRatio*sRatio;
			canvas.height = ih*pRatio*sRatio;
			canvas.style.width = iw*sRatio+'px';
			canvas.style.height = ih*sRatio+'px';
			stage.scaleX = pRatio*sRatio;
			stage.scaleY = pRatio*sRatio;
			lastW = iw; lastH = ih; lastS = sRatio;
		}
	})(false,'both',false,1);
}

init();
