(function () {
  var toggle = document.getElementById("menu-toggle");
  var menuContainer = document.getElementById("menu-container");
  var menu = document.getElementById("menu");
  var content = document.getElementById("content");

  var toggleClasses = function(){
    toggle.classList.toggle("on");
    menuContainer.classList.toggle("on");
    menu.classList.toggle("hidden");
    content.classList.toggle("blur");
  };
  toggle.addEventListener("click", toggleClasses);
  menu.addEventListener("click", toggleClasses);
})();
