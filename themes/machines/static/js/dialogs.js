document.addEventListener('DOMContentLoaded', function() {
  document.getElementById('planetWebDevelopment').addEventListener('click', function(e) {
    Custombox.open({
      target: '#webDevelopmentModal',
      effect: 'blur',
      overlayColor: '#296d86',
      overlayOpacity: 0.5
    });
  });
  document.getElementById('planetDesign').addEventListener('click', function(e) {
    Custombox.open({
      target: '#designModal',
      effect: 'blur',
      overlayColor: '#296d86',
      overlayOpacity: 0.5
    });
  });
  document.getElementById('planetDigitalMarketing').addEventListener('click', function(e) {
    Custombox.open({
      target: '#digitalModal',
      effect: 'blur',
      overlayColor: '#296d86',
      overlayOpacity: 0.5
    });
  });
  document.getElementById('planetMobileWeb').addEventListener('click', function(e) {
    Custombox.open({
      target: '#mobileWebModal',
      effect: 'blur',
      overlayColor: '#296d86',
      overlayOpacity: 0.5
    });
  });
  document.getElementById('planetContentCreation').addEventListener('click', function(e) {
    Custombox.open({
      target: '#contentCreationModal',
      effect: 'blur',
      overlayColor: '#296d86',
      overlayOpacity: 0.5
    });
  });
  document.getElementById('planetAnimation').addEventListener('click', function(e) {
    Custombox.open({
      target: '#animationModal',
      effect: 'blur',
      overlayColor: '#296d86',
      overlayOpacity: 0.5
    });
  });
  document.getElementById('planetUX').addEventListener('click', function(e) {
    Custombox.open({
      target: '#UXModal',
      effect: 'blur',
      overlayColor: '#296d86',
      overlayOpacity: 0.5
    });
  });
});
